import pygame
from configs import Config
from pygame.math import Vector2


class Ground(pygame.sprite.Sprite):
    def __init__(self, game):
        super().__init__()
        self.surf = pygame.Surface((Config.Screen.WIDTH, 100))
        self.surf.fill(Config.Colors.GREEN)
        self.image = self.surf.convert()
        self.rect = self.surf.get_rect(
            center=(Config.Screen.WIDTH/2, Config.Screen.HEIGHT-50))

        self.vel = Vector2(0, 0)

        game.allEntities.add(self)

    def update(self, game):
        self.rect = self.rect.move(self.vel)
