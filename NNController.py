from pygame import Rect
import numpy as np
from enum import Enum, auto
from typing import List, Tuple
import pygame
import torch
from torch import nn
from torch.nn import Sequential
from torch.nn import Linear
from torch.nn import ReLU, Softmax
from configs import Config


class NNOutput(Enum):
    DO_NOTHING = auto()
    JUMP = auto()


class NNInput():
    def __init__(self, obstacles: List[pygame.sprite.Sprite]):
        first_obstacle: Rect = obstacles[0].rect
        self.tensor = tensor = torch.FloatTensor(
            1, 3)
        tensor[0][0] = first_obstacle.left
        tensor[0][1] = first_obstacle.right
        tensor[0][2] = first_obstacle.top

    def get_tensor(self):
        return self.tensor


class NNController(nn.Module):

    def init_torch():
        torch.set_grad_enabled(False)

    def __init__(self):
        super().__init__()
        self.fc = Sequential(
            Linear(3, 4, bias=True),
            ReLU(),
            Linear(4, 2, bias=True),
            Softmax(dim=1)
        )

        for param in self.fc.parameters():
            param.requires_grad = False

    def forward(self, inputs: NNInput) -> NNOutput:
        in_tensor = inputs.get_tensor()
        out_tensor = self.fc(in_tensor)
        # print(out_tensor)
        choice = np.argmax(out_tensor)
        if choice == 0:
            return NNOutput.DO_NOTHING
        if choice == 1:
            return NNOutput.JUMP

    def mutate(self):
        for param in self.fc.parameters():
            if len(param.shape) == 2:
                for i0 in range(param.shape[0]):
                    for i1 in range(param.shape[1]):
                        param[i0][i1] += np.random.randn() * \
                            Config.NNController.MUT_POWER

            if len(param.shape) == 1:
                for i0 in range(param.shape[0]):
                    param[i0] += np.random.randn() * \
                        Config.NNController.MUT_POWER


if __name__ == "__main__":
    c = NNController()
    c.forward(NNInput(Vector2(12, 10)))
    c.mutate()
