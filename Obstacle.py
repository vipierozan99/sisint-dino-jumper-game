from random import randint
import pygame
from configs import Config
from pygame.math import Vector2
import random


class Obstacle(pygame.sprite.Sprite):
    count = 0

    def __init__(self, game):
        super().__init__()
        self.surf = pygame.Surface((randint(Config.Obstacle.MIN_W, Config.Obstacle.MAX_W), randint(
            Config.Obstacle.MIN_H, Config.Obstacle.MAX_H)))
        self.surf.fill(Config.Colors.RED)
        self.image = self.surf.convert()
        self.rect = self.surf.get_rect()
        self.rect.bottomright = (Config.Screen.WIDTH, game.ground.rect.top)

        self.vel = Vector2(Config.Obstacle.VEL)
        self.id = Obstacle.count
        Obstacle.count += 1

        game.allEntities.add(self)
        game.obstacles.add(self)

    def update(self, game):
        self.rect = self.rect.move(self.vel)
        if self.rect.right < 0:
            self.kill()

    def __str__(self):
        return f"Obstacle {self.id}"


class ObstacleTimer():
    def __init__(self, game):
        self.time_ms = Config.ObstacleTimer.MIN_TIME

    def update(self, game):
        self.time_ms -= game.clock.get_time()
        if self.time_ms <= 0:
            self.time_ms = random.randint(
                Config.ObstacleTimer.MIN_TIME, Config.ObstacleTimer.MAX_TIME)
            self.spawn_obstacle(game)

    def spawn_obstacle(self, game):
        return Obstacle(game)
