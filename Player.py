from random import randint
from Obstacle import Obstacle
import pygame
from pygame import Color, Rect, Surface
from pygame.sprite import Group, Sprite, collide_rect, groupcollide, spritecollideany
from configs import Config
from pygame.math import Vector2
from utils import tint_image, load_image, random_color
from NNController import *
from typing import List
import copy


class Player(pygame.sprite.Sprite):
    count = 0

    def __str__(self) -> str:
        return f"Player {self.id} time_alive {self.time_alive} record {self.record_time_alive}"

    def __init__(self, game):
        super().__init__()
        self.time_alive = 0
        self.record_time_alive = 0
        self.id = Player.count
        Player.count += 1

        self.image: Surface = pygame.transform.scale(
            load_image("dino.png"), (80, 80))
        self.tint_color = random_color()
        tint_image(self.image, self.tint_color)
        self.image.blit(game.font.render(
            f"{self.id}", False, Config.Colors.WHITE), (0, 0))

        self.rect: Rect = self.image.get_rect()
        self.rect.midleft = (Config.Screen.WIDTH/15, Config.Screen.HEIGHT/2)

        self.vel = Vector2(0, 0)
        self.accel = Vector2(0, Config.GRAVITY)
        self.on_ground = False

        game.allEntities.add(self)
        game.players.add(self)

        self.controller = NNController()

    def update(self, game) -> None:
        self.vel += self.accel
        self.rect.move_ip(self.vel)
        self.time_alive += game.clock.get_time()
        self.check_ground_coll(game)
        self.check_obstacle_coll(game)

        obst_by_dist = Player.get_obstacles_by_distance(game)
        action = NNOutput.DO_NOTHING
        if not len(obst_by_dist):
            return
        action = self.controller.forward(NNInput(obst_by_dist))
        if action == NNOutput.DO_NOTHING:
            return
        if action == NNOutput.JUMP:
            self.jump(game)

    def jump(self, game) -> None:
        if not self.on_ground:
            return
        self.on_ground = False
        self.vel = Config.Player.JUMP_VEL

    def check_ground_coll(self, game) -> None:
        if self.rect.colliderect(game.ground):
            self.on_ground = True
            self.rect.bottom = game.ground.rect.top
            self.vel = Vector2(0, 0)

    def check_obstacle_coll(self, game) -> None:
        if spritecollideany(self, game.obstacles):
            game.allEntities.remove(self)
            game.dead_players.add(self)
            game.players.remove(self)

    def get_obstacles_by_distance(game) -> List[Obstacle]:
        obstacles: List[Sprite] = game.obstacles.sprites()
        obstacles.sort(key=lambda x: x.rect.left)
        return obstacles

    def clone(self, game):
        clone = Player(game)
        clone.controller = copy.deepcopy(self.controller)
        return clone

    def mutate(self):
        self.controller.mutate()

    def reset_time_alive(self):
        if self.time_alive > self.record_time_alive:
            self.record_time_alive = self.time_alive
        self.time_alive = 0

    def revive(self, game):
        game.allEntities.add(self)
        game.players.add(self)
