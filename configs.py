from enum import Enum


class Config:
    FPS = 60
    GRAVITY = 1
    GENERATION_SIZE = 30
    KEEP_SIZE = 3
    MUTATE_SIZE = 15

    class NNController():
        MUT_POWER = 0.02

    class Screen():
        WIDTH = 1200
        HEIGHT = 600
        SIZE = (WIDTH, HEIGHT)

    class Colors():
        WHITE = (255, 255, 255)
        BLACK = (0, 0, 0)
        BLUE = (0, 0, 255)
        RED = (255, 0, 0)
        GREEN = (0, 255, 0)

    class Player():
        JUMP_VEL = (0, -22)

    class Obstacle():
        VEL = (-8, 0)
        MIN_W = 50
        MAX_W = 100
        MIN_H = 50
        MAX_H = 100

    class ObstacleTimer:
        MIN_TIME = 800  # ms
        MAX_TIME = 2000  # ms
