from pygame.sprite import Group, RenderUpdates
from Obstacle import Obstacle, ObstacleTimer
from configs import Config
import pygame
from pygame.constants import K_ESCAPE, K_LSHIFT, K_SPACE, K_UP, K_d, K_p
# from pygame.locals import *
from Player import Player
from Ground import Ground
from pygame import time, font
from pygame.sprite import Group


class Game:
    def __init__(self):
        self._running = True
        self._paused = False
        self.epoch = 0

    def on_init(self):
        pygame.init()
        self.screen = pygame.display.set_mode(
            Config.Screen.SIZE, pygame.HWSURFACE | pygame.DOUBLEBUF)
        pygame.display.set_caption("EVO_DINO")
        self.font = font.SysFont(None, 48)
        self._running = True
        self.clock = time.Clock()
        self.shift_pressed = False

        self.background = pygame.Surface(self.screen.get_size()).convert()
        self.background.fill(Config.Colors.BLACK)

        self.screen.blit(self.background, (0, 0))

        self.allEntities = RenderUpdates()
        self.obstacles: Group = Group()
        self.players = Group()
        self.dead_players = Group()

        for _ in range(Config.GENERATION_SIZE):
            Player(self)

        self.ground = Ground(self)
        self.obstacle_timer = ObstacleTimer(self)

    def on_event(self, event):
        if event.type == pygame.QUIT:
            self._running = False

        if event.type == pygame.KEYDOWN and event.key == K_LSHIFT:
            self.shift_pressed = True
        if event.type == pygame.KEYUP and event.key == K_LSHIFT:
            self.shift_pressed = False

        if event.type == pygame.KEYDOWN:
            self.handle_keys(event)

    def handle_keys(self, event):
        if event.key == K_ESCAPE:
            self._running = False

        if event.key in (K_UP, K_SPACE):
            for player in self.players:
                player.jump(self)

        if event.key == K_p:
            self._paused = not self._paused

        if event.key == K_d:
            print(self.players)

    def on_loop(self):
        self.clock.tick(Config.FPS)
        self.obstacle_timer.update(self)
        self.allEntities.update(self)

    def on_render(self):
        self.allEntities.clear(self.screen, self.background)
        self.allEntities.draw(self.screen)
        pygame.display.update()

    def on_cleanup(self):
        pygame.quit()

    def on_execute(self):
        if self.on_init() is False:
            self._running = False

        while(self._running):
            for event in pygame.event.get():
                self.on_event(event)
            if not self._paused:
                self.on_loop()
                self.on_render()

            if len(self.players) == 0:
                self.next_epoch()

        self.on_cleanup()

    def next_epoch(self):
        # sort players by time_alive, separate in [G1,G2,G3], keep G1, mutate G2 and randomize G3
        # increase epoch
        # run again
        for e in self.obstacles:
            e.kill()

        ordered_players = sorted(
            self.dead_players, key=lambda x: x.time_alive, reverse=True)

        print(f"Epoch {self.epoch}")
        print(f"New Players are:")
        for e in ordered_players[0:Config.KEEP_SIZE]:
            e.revive(self)
            print(f"{str(e)} KEPT")

        for e in ordered_players[Config.KEEP_SIZE:Config.KEEP_SIZE +
                                 Config.MUTATE_SIZE]:
            e.mutate()
            e.revive(self)
            print(f"{str(e)} MUTATED")

        for e in ordered_players[Config.KEEP_SIZE +
                                 Config.MUTATE_SIZE:]:
            e.kill()
            p = Player(self)
            print(f"{str(p)} NEW")

        self.dead_players.empty()
        self.epoch += 1

        for e in self.players:
            e.reset_time_alive()


if __name__ == "__main__":
    theGame = Game()
    theGame.on_execute()
