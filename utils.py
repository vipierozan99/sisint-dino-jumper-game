from random import randint
from typing import Tuple
import pygame
import os
from pygame import Color, Rect, Surface


def random_color() -> Color:
    return Color(randint(0, 255), randint(0, 255), randint(0, 255))


def avg_color(a: Color, b: Color, avg_a=False) -> Color:
    return Color(int((a.r + b.r)/2), int((a.g + b.g)/2), int((a.b + b.b)/2), a.a)


def tint_image(surface: Surface, color: Color) -> None:
    """Tints all pixels of the surface with color, preserve transparency."""
    w, h = surface.get_size()
    for x in range(w):
        for y in range(h):
            surface.set_at((x, y), avg_color(surface.get_at((x, y)), color))


def load_image(name: str, alpha=True, colorkey=None) -> Surface:
    fullname = os.path.join('assets', name)
    try:
        image = pygame.image.load(fullname)
    except pygame.error as message:
        print('Cannot load image:', name)
        raise SystemExit(message)
    if alpha:
        image = image.convert_alpha()
    else:
        image = image.convert()
    if colorkey is not None:
        if colorkey == -1:
            colorkey = image.get_at((0, 0))
        image.set_colorkey(colorkey, pygame.RLEACCEL)
    return image
